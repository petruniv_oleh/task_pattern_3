package com.oleh.flowers.view;

@FunctionalInterface
public interface Printable {
    void print() throws IllegalAccessException;
}
