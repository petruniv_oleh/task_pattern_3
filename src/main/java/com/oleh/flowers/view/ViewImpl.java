package com.oleh.flowers.view;

import com.oleh.flowers.controller.Controller;
import com.oleh.flowers.controller.ControllerImpl;
import com.oleh.flowers.model.client.Card;
import com.oleh.flowers.model.client.Client;
import com.oleh.flowers.model.flowersShop.Order;
import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;
import com.oleh.flowers.model.flowersShop.bouquet.Event;
import com.oleh.flowers.model.flowersShop.bouquet.Flower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ViewImpl implements View {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger("ViewLoger");
    Scanner scanner = new Scanner(System.in);
    private Client client;


    public ViewImpl() {
        controller = new ControllerImpl();
        logIn();

        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Add to order");
        menuMap.put("2", "2. Confirm order");
        menuMap.put("3", "3. Show order");

        menuMapMethods.put("1", this::addToOrder);
        menuMapMethods.put("2", this::confirmOrder);
        menuMapMethods.put("3", this::showOrder);

    }


    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void logIn() {
        logger.debug("Invoke logIn");
        System.out.println("Please log in");
        System.out.println("Enter your name");
        String name = scanner.nextLine();
        Card card = chooseCard();
        if (card == null) {
            System.out.println("Wrong card type!");
            card = Card.NO;
        }

        client = new Client(card, name);

    }

    private Card chooseCard() {
        logger.debug("Invoke chooseCard");
        System.out.println("Which card do you have? (GOLD, SILVER, SOCIAL, PLATINUM, NO");
        String cardName = scanner.nextLine().toUpperCase();

        switch (cardName) {
            case "GOLD":
                return Card.GOLD;
            case "SILVER":
                return Card.SILVER;
            case "SOCIAL":
                return Card.SOCIAL;
            case "PLATINUM":
                return Card.PLATINUM;
            case "NO":
                return Card.NO;
            default:
                return null;
        }

    }

    @Override
    public void addToOrder() {
        logger.debug("Invoke addToOrder");
        System.out.println("Choose an option:");
        System.out.println("1. Add from catalogue");
        System.out.println("2. Create custom");
        System.out.println("===================");
        String line = scanner.nextLine();
        switch (Integer.parseInt(line)) {
            case 1:
                fromCatalogue();
                break;
            case 2:
                createBouquet();
                break;
            default:
                System.out.println("Wrong option");
                break;
        }


    }

    private void fromCatalogue() {
        logger.debug("Invoke fromCatalogue");
        Map<Integer, Bouquet> catalogue = controller.getCatalogue();

        Set<Integer> keys = catalogue.keySet();

        for (int key : keys
        ) {
            System.out.println(key + catalogue.get(key).toString());
        }


        System.out.println("Your choise: ");
        String choise = scanner.nextLine();
        if (keys.contains(Integer.parseInt(choise))) {
            Bouquet bouquet = catalogue.get(Integer.parseInt(choise));
            controller.addToOrder(client,bouquet.getBouquetName(), bouquet.getFlowers(), bouquet.getEvent());

        } else {
            System.out.println("Illegal!");
            return;
        }


    }

    private void createBouquet() {
        logger.debug("Invoke createBouquet");
        List<Flower> flowers = new ArrayList<>();
        addFlower(flowers);
        while (true){
            System.out.println("Do you want to add one more flower?(y/n)");
            String line = scanner.nextLine();
            if (line.equals("y")){
                addFlower(flowers);
            }else{
                break;
            }
        }
        Event event = chooseEvent();
        if (event == null) {
            System.out.println("Wrong event!!!");
            return;
        }
        System.out.println("Enter the name of your bouquet");
        String name = scanner.nextLine();
        controller.addToOrder(client, name, flowers, event);
    }

    private void addFlower(List<Flower> flowers) {
        logger.debug("Invoke addFlower");
        System.out.println("Choose what flower would be in your bouquet" +
                "ROSE, ROMASHKA, TULPAN, LILIA, VIOLETS;");
        String flower = scanner.nextLine().toUpperCase();

        switch (flower) {
            case "ROSE":
                flowers.add(Flower.ROSE);
                break;
            case "ROMASHKA":
                flowers.add(Flower.ROMASHKA);
                break;
            case "TULPAN":
                flowers.add(Flower.TULPAN);
                break;
            case "LILIA":
                flowers.add(Flower.LILIA);
                break;
            case "VIOLETS":
                flowers.add(Flower.VIOLETS);
                break;
            default:
                System.out.println("Wrong flower");
                break;
        }
    }

    private Event chooseEvent() {
        logger.debug("Invoke chooseEvent");
        System.out.println("Choose event ( FUNERAL, WEDDING, BIRTHDAY, VALENTINES, CUSTOM): ");
        String event = scanner.nextLine().toUpperCase();

        switch (event) {
            case "FUNERAL":
                return Event.FUNERAL;
            case "WEDDING":
                return Event.WEDDING;
            case "BIRTHDAY":
                return Event.BIRTHDAY;
            case "VALENTINES":
                return Event.VALENTINES_DAY;
            case "CUSTOM":
                return Event.CUSTOM;
            default:
                return null;
        }

    }

    @Override
    public void showOrder() {
        logger.debug("Invoke showOrder");
        Order order = controller.getOrder();
        System.out.println(order);
    }

    @Override
    public void confirmOrder() {
        logger.debug("Invoke confirmOrder");
        System.out.println("Do you want to add shipping to your order? (y/n)");
        String shipping = scanner.nextLine().toLowerCase();
        if (shipping.equals("y")){
            controller.addShipping();
        }
        System.out.println("Do you want to add packing to your order? (y/n)");
        String packing = scanner.nextLine().toLowerCase();
        if (packing.equals("y")){
            controller.addPacking();
        }
        System.out.println("Thank for your ordering! Please wait...");
        Order order = controller.getOrder();
        System.out.println("Your order: " +
                "\nbouquets: "+order.getBouquets()+
                "\nservices: "+order.getAdditionalServices());
        controller.confirmOrder();
    }
}
