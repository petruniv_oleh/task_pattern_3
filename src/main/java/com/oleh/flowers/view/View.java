package com.oleh.flowers.view;

public interface View {
    void showMenu();
    void logIn();
    void addToOrder();
    void showOrder();
    void confirmOrder();
}
