package com.oleh.flowers.model.flowersShop.factory;

import com.oleh.flowers.model.flowersShop.bouquet.*;

import java.util.List;

public class WeddingBouquet extends BouquetFactory {

    @Override
    public Bouquet createBouquet(String bouquetName, List<Flower> flowerList) {
        Bouquet bouquet = new Bouquet();
        System.out.println("Making wedding bouquet");
        bouquet.setBouquetName(bouquetName);
        bouquet.setFlowers(flowerList);
        bouquet.setEvent(Event.WEDDING);
        bouquet.setWraping(Wraping.PAPER);
        return bouquet;
    }
}
