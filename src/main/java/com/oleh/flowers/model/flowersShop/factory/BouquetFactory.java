package com.oleh.flowers.model.flowersShop.factory;

import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;
import com.oleh.flowers.model.flowersShop.bouquet.Flower;

import java.util.List;

public abstract class BouquetFactory {

    public abstract Bouquet createBouquet(String bouquetName, List<Flower> flowerList);

    public Bouquet assamble(String bouquetName, List<Flower> flowerList){
        Bouquet bouquet = createBouquet(bouquetName, flowerList);
        System.out.println("bouquet is almost ready ready");
        return bouquet;
    }
}
