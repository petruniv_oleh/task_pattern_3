package com.oleh.flowers.model.flowersShop.decorator;


import com.oleh.flowers.model.flowersShop.Order;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Shipping extends OrderDecorator {

    public Shipping(Order order) {
        setOrder(order);
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(new File("src/main/resources/services.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setAdditionalService("Shipping");
        if (isFreeShipping()){
            setPrice(0);
        }else {
            setPrice(Integer.parseInt(properties.getProperty("shipping")));
        }

    }


}
