package com.oleh.flowers.model.flowersShop;

import com.oleh.flowers.model.client.Client;
import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class OrderImpl implements Order {

    private List<Bouquet> bouquets;
    private Client client;
    private int price;
    private List<String> additionalServices;

    public OrderImpl() {
        bouquets = new ArrayList<>();
        additionalServices = new ArrayList<>();

    }

    public OrderImpl(List<Bouquet> bouquets, Client client, int price, List<String> additionalServices) {
        this.bouquets = bouquets;
        this.client = client;
        this.price = price;
        this.additionalServices = additionalServices;
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setAdditionalServices(List<String> additionalServices) {
        this.additionalServices = additionalServices;
    }

    @Override
    public int getDiscount() {
        return client.getCard().getDiscount();
    }

    @Override
    public boolean isFreeShipping() {
        return client.getCard().isFreeShipping();
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public List<String> getAdditionalServices() {
        return additionalServices;
    }


    @Override
    public String toString() {
        StringBuilder listOfBouquets = new StringBuilder();
        StringBuilder listOfServices = new StringBuilder();

        for (Bouquet b : bouquets
        ) {
            listOfBouquets.append(b.getBouquetName() + ", ");
        }

        for (String s : additionalServices
        ) {
            listOfServices.append(s + ", ");
        }


        return "OrderImpl{" +
                "bouquets=" + listOfBouquets.toString() +
                ", client=" + client +
                ", price=" + price +
                ", additionalServices=" + listOfServices.toString() +
                '}';
    }
}
