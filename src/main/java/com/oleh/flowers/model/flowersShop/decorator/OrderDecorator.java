package com.oleh.flowers.model.flowersShop.decorator;

import com.oleh.flowers.model.client.Client;
import com.oleh.flowers.model.flowersShop.Order;
import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;

import java.util.List;
import java.util.Optional;

public class OrderDecorator implements Order {

    private Optional<Order> order;
    private String additionalService;
    private int price;

    public void setOrder(Order outOrder) {
        order = Optional.ofNullable(outOrder);

    }

    public void setAdditionalService(String additionalService) {
        this.additionalService = additionalService;
        order.orElseThrow(IllegalArgumentException::new).getAdditionalServices().add(additionalService);
    }

    public void setPrice(int price) {
        this.price = price;
    }


    @Override
    public List<Bouquet> getBouquets() {
        return order.orElseThrow(IllegalArgumentException::new).getBouquets();
    }

    @Override
    public int getPrice() {
        return order.orElseThrow(IllegalArgumentException::new).getPrice()+price;
    }

    @Override
    public List<String> getAdditionalServices() {
        return order.orElseThrow(IllegalArgumentException::new).getAdditionalServices();
    }

    @Override
    public Client getClient() {
        return order.orElseThrow(IllegalArgumentException::new).getClient();
    }

    @Override
    public int getDiscount() {
        return order.orElseThrow(IllegalArgumentException::new).getDiscount();
    }

    @Override
    public boolean isFreeShipping() {
        return order.orElseThrow(IllegalArgumentException::new).isFreeShipping();
    }



    @Override
    public String toString() {
        StringBuilder listOfBouquets = new StringBuilder();
        StringBuilder listOfServices = new StringBuilder();
        List<Bouquet> bouquets = getBouquets();
        List<String> additionalServices = getAdditionalServices();
        for (Bouquet b: bouquets
        ) {
            listOfBouquets.append(b.getBouquetName()+", ");
        }

        for (String s: additionalServices
        ) {
            listOfServices.append(s+", ");
        }


        return "Order \n" +
                "bouquets=" + listOfBouquets.toString() +
                ", price=" + price +
                ", additionalServices=" + listOfServices.toString() +
                '}';
    }
}
