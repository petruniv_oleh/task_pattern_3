package com.oleh.flowers.model.flowersShop.bouquet;

public enum Event {
    FUNERAL, WEDDING, BIRTHDAY, VALENTINES_DAY, CUSTOM;
}
