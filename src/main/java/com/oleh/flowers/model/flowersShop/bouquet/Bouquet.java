package com.oleh.flowers.model.flowersShop.bouquet;

import java.util.List;

public class Bouquet {
    private String bouquetName;
    private List<Flower> flowers;
    private Event event;
    private Wraping wraping;


    public Bouquet() {
    }

    public Bouquet(String bouquetName, List<Flower> flowers, Event event, Wraping wraping) {
        this.bouquetName = bouquetName;
        this.flowers = flowers;
        this.event = event;
        this.wraping = wraping;
    }

    public String getBouquetName() {
        return bouquetName;
    }

    public void setBouquetName(String bouquetName) {
        this.bouquetName = bouquetName;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Wraping getWraping() {
        return wraping;
    }

    public void setWraping(Wraping wraping) {
        this.wraping = wraping;
    }

    public int getFlowersPrice() {
        int sum = 0;
        for (Flower f :
                flowers) {
            sum += f.getFlowerPrice();
        }
        return sum;
    }


    @Override
    public String toString() {
        return "\nBouquet: " +
                "\nname:" + bouquetName +
                "\nflowers: " + flowers +
                "\nevent: " + event +
                "\nwraping: " + wraping;

    }
}
