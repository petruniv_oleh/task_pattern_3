package com.oleh.flowers.model.flowersShop;

import com.oleh.flowers.model.client.Client;
import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;

import java.util.List;

public interface Order {
    int getPrice();
    List<String> getAdditionalServices();
    int getDiscount();
    boolean isFreeShipping();
    List<Bouquet> getBouquets();
    Client getClient();

}
