package com.oleh.flowers.model.flowersShop.bouquet;

public enum Flower {
    ROSE(40), ROMASHKA(15), TULPAN(30),
    LILIA(35), VIOLETS(20);

    private int flowerPrice;

    Flower(int flowerPrice) {
        this.flowerPrice = flowerPrice;
    }

    public int getFlowerPrice() {
        return flowerPrice;
    }
}
