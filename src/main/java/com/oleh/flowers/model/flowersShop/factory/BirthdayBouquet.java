package com.oleh.flowers.model.flowersShop.factory;

import com.oleh.flowers.model.flowersShop.bouquet.*;

import java.util.List;

public class BirthdayBouquet extends BouquetFactory {

    @Override
    public Bouquet createBouquet(String bouquetName, List<Flower> flowerList) {
        Bouquet bouquet = new Bouquet();
        System.out.println("Making birthday bouquet");
        bouquet.setBouquetName(bouquetName);
        bouquet.setFlowers(flowerList);
        bouquet.setEvent(Event.BIRTHDAY);
        bouquet.setWraping(Wraping.PAPER);

        return bouquet;
    }
}
