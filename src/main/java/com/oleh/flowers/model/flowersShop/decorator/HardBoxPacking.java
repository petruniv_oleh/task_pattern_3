package com.oleh.flowers.model.flowersShop.decorator;


import com.oleh.flowers.model.flowersShop.Order;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class HardBoxPacking extends OrderDecorator {

    public HardBoxPacking(Order order) {
        setOrder(order);
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(new File("src/main/resources/services.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setAdditionalService("Hard box packing");
        setPrice(Integer.parseInt(properties.getProperty("boxPacking")));
    }
}
