package com.oleh.flowers.model.flowersShop.factory;

import com.oleh.flowers.model.flowersShop.bouquet.*;

import java.util.List;

public class ValentinesDayBouquet extends BouquetFactory {

    @Override
    public Bouquet createBouquet(String bouquetName, List<Flower> flowerList) {
        Bouquet bouquet = new Bouquet();
        System.out.println("Making Valentines Day bouquet");
         bouquet.setBouquetName(bouquetName);
         bouquet.setFlowers(flowerList);
         bouquet.setEvent(Event.VALENTINES_DAY);
         bouquet.setWraping(Wraping.PAPER);
        return bouquet;
    }
}
