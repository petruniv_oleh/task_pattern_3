package com.oleh.flowers.model.client;

import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;

import java.util.List;

public class Client {
    private Card card;
    private String name;


    public Client() {
    }

    public Client(Card card, String name) {
        this.card = card;
        this.name = name;

    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Client{" +
                "card=" + card +
                ", name='" + name + '\'' +
                '}';
    }
}
