package com.oleh.flowers.model.client;

public enum Card {
    GOLD(20, false), SILVER(10, false),
    SOCIAL(15, true), PLATINUM(25, true),
    NO(0,false);

    private int discount;
    private boolean freeShipping;
    Card(int discount, boolean freeShipping) {
        this.discount = discount;
        this.freeShipping =freeShipping;
    }

    public int getDiscount() {
        return discount;
    }

    public boolean isFreeShipping() {
        return freeShipping;
    }
}
