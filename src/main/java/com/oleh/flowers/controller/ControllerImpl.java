package com.oleh.flowers.controller;

import com.oleh.flowers.model.client.Client;
import com.oleh.flowers.model.flowersShop.Order;
import com.oleh.flowers.model.flowersShop.OrderImpl;
import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;
import com.oleh.flowers.model.flowersShop.bouquet.Event;
import com.oleh.flowers.model.flowersShop.bouquet.Flower;
import com.oleh.flowers.model.flowersShop.bouquet.Wraping;
import com.oleh.flowers.model.flowersShop.decorator.HardBoxPacking;
import com.oleh.flowers.model.flowersShop.decorator.OrderDecorator;
import com.oleh.flowers.model.flowersShop.decorator.Shipping;
import com.oleh.flowers.model.flowersShop.factory.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ControllerImpl implements Controller {

    OrderImpl simpleOrder = new OrderImpl();
    OrderDecorator decoratedOrder;
    private static Logger logger = LogManager.getLogger("ObserverLog");

    @Override
    public void addToOrder(Client client, String name, List<Flower> flowers, Event event) {
        BouquetFactory bouquetFactory = null;
        switch (event) {
            case FUNERAL:
                bouquetFactory = new FuneralBouquet();
                break;
            case BIRTHDAY:
                bouquetFactory = new BirthdayBouquet();
                break;
            case VALENTINES_DAY:
                bouquetFactory = new ValentinesDayBouquet();
                break;
            case WEDDING:
                bouquetFactory = new WeddingBouquet();
                break;
            case CUSTOM:
                bouquetFactory = new CustomBouquet();
                break;
        }
        if (bouquetFactory != null) {
            Bouquet assamble = bouquetFactory.assamble(name, flowers);
            List<Bouquet> bouquets = simpleOrder.getBouquets();
            simpleOrder.setPrice(assamble.getFlowersPrice());
            bouquets.add(assamble);
            simpleOrder.setBouquets(bouquets);
            simpleOrder.setClient(client);
        }

    }

    @Override
    public Map<Integer, Bouquet> getCatalogue() {
        Map<Integer, Bouquet> bouquets = new LinkedHashMap<>();
        List<Flower> flowers = new ArrayList<>();
        flowers.add(Flower.ROSE);
        flowers.add(Flower.TULPAN);

        bouquets.put(1, new Bouquet("Luxury prince", flowers, Event.BIRTHDAY, Wraping.PAPER));
        List<Flower> flowers2 = new ArrayList<>();
        flowers2.add(Flower.ROMASHKA);
        bouquets.put(2, new Bouquet("Diamont snow", flowers2, Event.WEDDING, Wraping.PAPER));


        return bouquets;
    }

    @Override
    public Order getOrder() {
        if (decoratedOrder == null) {
            return simpleOrder;
        } else {
            return decoratedOrder;
        }
    }

    @Override
    public void addShipping() {
        OrderDecorator shipping;
        if (decoratedOrder == null) {
            shipping = new Shipping(simpleOrder);
        } else {
            shipping = new Shipping(decoratedOrder);
        }
        decoratedOrder = shipping;
    }

    @Override
    public void addPacking() {
        OrderDecorator packing;
        if (decoratedOrder == null) {
            packing = new HardBoxPacking(simpleOrder);
        } else {
            packing = new HardBoxPacking(decoratedOrder);
        }
        decoratedOrder = packing;
    }

    @Override
    public void confirmOrder() {

        if (decoratedOrder==null) {
            int sPrice = simpleOrder.getPrice();
            logger.info("The order for " + simpleOrder.getClient().getName() + " is ready now! The price is" +
                    (sPrice - simpleOrder.getDiscount()*0.01* sPrice)+ "");
        }else {
            int dPrice = decoratedOrder.getPrice();
            logger.info("The order for " + decoratedOrder.getClient().getName() + " is ready now! The price is " +
                    (dPrice- decoratedOrder.getDiscount()*0.01* dPrice) + "");
        }
        simpleOrder = new OrderImpl();
        decoratedOrder = null;
    }
}
