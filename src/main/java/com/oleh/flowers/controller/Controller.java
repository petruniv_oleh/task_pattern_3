package com.oleh.flowers.controller;

import com.oleh.flowers.model.client.Client;
import com.oleh.flowers.model.flowersShop.Order;
import com.oleh.flowers.model.flowersShop.bouquet.Bouquet;
import com.oleh.flowers.model.flowersShop.bouquet.Event;
import com.oleh.flowers.model.flowersShop.bouquet.Flower;

import java.util.List;
import java.util.Map;

public interface Controller {

    Map<Integer,Bouquet> getCatalogue();

    void addToOrder(Client client, String name, List<Flower> flowers, Event event);

    Order getOrder();

    void addShipping();

    void addPacking();

    void confirmOrder();
}
